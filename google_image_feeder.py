#!/usr/bin/python
# -*- coding: utf-8 -*-
#Instalacion:
# Se ha programado para la version Python 2.x
# Por defecto, python es posible que no incluya ciertas librerias, para eso es necesario usar el comando pip (incluido
# en la distribucion) e instalarlos. 
# #pip install wget
# #pip install google-api-python-client 
#Para utilizar las API de búsquedas de google, principalmente se tienen que tener activados dos servicios de Google:
# - El servicio de APIs para desarrolladores. y asi conseguiremos la key (developerkey).
# - Activar una búsqueda personalizada habilitada para buscar imagenes (que lo pasaremos mediante le parametro cx).
# Estos parametros se guardaran en el fichero de llaves con una separacion de # (por defecto keys.conf)
# Para el primer paso, entramos en la web https://code.google.com/apis/console/ y creamos un proyecto de desarrollo.
# - Accedemos a la web y vamos a la pestaña de nuevo proyecto y creamos uno nuevo.. Cuando esté creado lo seleccionamos
#  vamos al apartado API, y en el buscado, buscamos 'Custom Search API', la seleccionamos y la habilitamos.
# - Despues, accedemos a la parte de credenciales y guardamos la API Key que sale, si no tenemos le damos a generar una nueva. Esta
#  llave sera nuestro variable developerkey(primer valor de la nueva linea a anadir en keys.conf).
# Para conseguir la variable cx y configurar la busqueda vamos a este enlace: http://www.google.com/cse/manage/all y 
# creamos un motor nuevo de busqueda...
# - En el motor de busqueda nuevo ponemos en "Sitios en los que ", "Busca en toda la Web, pero enfatizar los 
#   sitios incluidos.
# - en el apartado "buscar" se incluye la web www.google.com.
# - Se activa busqueda de imagenes
# - y finalmente le damos al boton id de motor de busqueda y nos quedamos con el valor (del tipo 00531...:nrg..),
#   este valor se la variable cx de nuestro fichero de llaves (segundo valor para poner en keys.conf, despues de la # del
# valor previo)
# Estos valores conseguidos (developerkey y cx), los ponemos en el fichero de llaves (keys.conf). Por defecto, 
# se usa feed.conf, pero puede hacerse otro y pasarse por parametro (con la opcion -i)


__author__ = 'carlos.baezruiz@gmail.com (Carlos Báez)'

import pprint
import ConfigParser, os
import sys
import argparse
import urllib
from urllib2 import HTTPError
from apiclient.discovery import build
import wget
import Queue
import threading
import time
import datetime

TIMEOUT=30
RESULTS_PER_PAGE=10



class MyStoppableThread(threading.Thread):
	def __init__(self,func, args=(), kwargs={}):
		threading.Thread.__init__(self)
		self.result = None
		self.func = func
		self.args = args
		self.kwargs = kwargs
	def run(self):
		self.result = self.func(*self.args,**self.kwargs)
	def _stop(self):
		if self.isAlive():
			threading.Thread._Thread__stop(self)

class TimeLimitExpired(Exception): pass


class DownloaderThread(threading.Thread):
    def __init__(self,timeout,func,args=(),kwargs={}):
	threading.Thread.__init__(self)
        self.timeout = timeout
	self.mythread = MyStoppableThread(func,args,kwargs)

    def run(self):
	self.mythread.start()
	self.mythread.join(self.timeout)
	if self.mythread.isAlive():
		self.mythread._stop()
		raise TimeLimitExpired()
	else:
		return self.mythread.result

#Class to save links
class FileSaver:
    def __init__(self,filename='db.txt'):
        self.filename=filename

    def save(self,url):
        with open(self.filename,'a+') as f:
            f.write(url+'\n')

class KeyReader:
    def __init__(self,fileConfig="keys.txt"):
        self.fileConfig = fileConfig
        with open(fileConfig) as f:
            content = f.readlines() 
        self.keys=[(value.split("#")[0].strip(),value.split("#")[1].strip()) for value in content]
        self.enables=[True for x in range(len(self.keys))]


#Clase para leer propiedades
class FileProperties:
    def __init__(self,fileconfig="feed.conf"):
        self.outdirectory = None
        if not os.path.isfile(fileconfig):
            raise Runtime("There are not configuration file")

        config = ConfigParser.ConfigParser()    
        config.readfp(open(fileconfig))

        if not config.has_section("google"):
            raise Runtime("There are not configuration file")

        if not config.has_option("google","maxelements"):
            raise Exception("It was not loaded correctly the file configuration")
        
        self.maxelements = int(config.get("google","maxelements"))
        self.outdirectory = config.get("google","outdirectory")
 


#Worker function, it is called as a thread worker
def worker_scrapper(searchterm,keyreader,queue,start_with,imgSizes=None):
	index = 0
	size = len(keyreader.enables)
	called = False
	while index < size and not called:
        	while not keyreader.enables[index]:
        	    print "Retrying call enables"
        	    index=index+1
        	    continue
	
	        pairkeys = keyreader.keys[index]
	        devKey = pairkeys[0]
	        cx = pairkeys[1]
		print "\nGetting index="+str(index)+" developerkey="+devKey+ " cx="+cx+'\n'
	        service = build("customsearch", "v1",developerKey=devKey)
		try:
			res = None
			if imgSizes!=None  and type(imgSizes) == list and len(imgSizes)>0:
				for imgSize in imgSizes:
					#print unicode(imgSize)
					res = service.cse().list(q=searchterm,searchType='image',cx=cx,start=start_with,imgSize=unicode(imgSize),).execute()
        		else:
				res = service.cse().list(q=searchterm,searchType='image',cx=cx,start=start_with,).execute()
			called = True
                        print "\nDownloaded correclty! with devkey="+devKey+" and cx="+cx+'\n'
		except Exception as error:
			print "\nUnknown error to call service: \n"
			print "\nTrying to download links from new keys\n"
                        print '\n'+str(error)+'\n'
			keyreader.enables[index]=False
			index=index+1
                        continue

                if res == None or not 'items' in res:
                    continue

		items = res['items']
        	if items!=None:
            		for item in items:
                		queue.put(item['link'])


#Main class google scrapper
class GoogleScrapper:
	HUGE = "huge"
	ICON = "icon"
	LARGE = "large"
	MEDIUM ="medium"
	XLARGE = "xlarge"
	XXLARGE = "xxlarge"
	def __init__(self,max_elements,output,keyreader):
        	self.max_elements = max_elements
        	self.output = output
        	self.q = Queue.Queue()
        	self.fileSaver = FileSaver()
        	self.keyreader = keyreader
		self.imgSizes=[]
	def addSize(self,newSize):
		self.imgSizes.append(newSize)
	def cleanSize(self):
		self.imgSizes = []

	#Funcion para extraer timestamp
	def __getTimeStamp(self):
		ts = time.time()
	        st = datetime.datetime.fromtimestamp(ts).strftime('%m%d_%H%M%S')
		return str(st)

	def run(self,searchTerm,start_with):
     		parsedSearchTerm = urllib.quote_plus(searchTerm)
        	total_values = self.max_elements - start_with

        	list_workers = []
                list_downloaders = []
	        print "Searching term="+parsedSearchTerm
	        for indexSearch in range(start_with,total_values,RESULTS_PER_PAGE):
	            print "Starting search from index="+str(indexSearch)
		    worker = threading.Thread(target=worker_scrapper,name="Worker",args=(parsedSearchTerm,self.keyreader,self.q,indexSearch,self.imgSizes,))
        	    worker.start()
        	    list_workers.append(worker)


        	destDirectory=str(self.output + os.sep + self.__getTimeStamp()+"_"+parsedSearchTerm)
		if not os.path.isdir(destDirectory):
        		os.makedirs(destDirectory)

		while (all([not worker.is_alive() for worker in list_workers])==False) or (self.q.qsize()>0):
                    try:
        	        link = self.q.get(True,10)
                    except:
                        #There are not more links to download
                        link = None
                        continue
        	    if link != None:
			try:
                                downloader = DownloaderThread(TIMEOUT,self.__download_links,(link,destDirectory))
                                downloader.start()
                                list_downloaders.append(downloader)

			except TimeLimitExpired as e:
				print e.message
			except Exception as e:
				print "It occurs an unknown error"
				print e.message


                while (all([not downloader.is_alive() for downloader in list_downloaders])==False):
                    time.sleep(1)
                
    	#Funcion para guardar links
    	def __download_links(self,link,destDirectory):
    		try:
        	        print "\nDownloading link: "+str(link)
    	        	wget.download(link,out=destDirectory)
       		#	self.fileSaver.save(link)
        	except Exception as e:
 	       	    print "\nIt was not possible download the link="+str(link)+'\n'



def main():
	
    parser = argparse.ArgumentParser(description='Google Images crawler')
    parser.add_argument("words", metavar='N',type=str,help="search to word")
    parser.add_argument('-o','--output',dest="output_directory",type=str,default="",)
    parser.add_argument('-i','--input',dest="input_config",type=str,default="feed.conf",)
    parser.add_argument('-s','--start',dest="start_with",type=int,default=1,)
    parser.add_argument('-S','--img_sizes',dest="img_sizes",nargs="+",type=str,default="",help="possible values: huge icon large medium xlarge xxlarge",)

    args = parser.parse_args()

    properties = FileProperties(args.input_config)
    if (args.output_directory != ""):
        properties.outdirectory = args.output_directory
   
    print "loaded properties file"
    keys = KeyReader()
    print "loaded keys num of keys="+str(len(keys.keys))
    scrapper = GoogleScrapper(properties.maxelements,properties.outdirectory,keys)

    if args.img_sizes != "":
        for size in args.img_sizes:
	    scrapper.addSize(size)

    scrapper.run(args.words,args.start_with)

if __name__ == '__main__':
    main()
