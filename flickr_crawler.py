#!/usr/bin/python
# sudo easy_install -U pip
# sudo pip install flickrapi
# sudo install wget
# sudo apt-get install python-dev libffi-dev libssl-dev <- if it is necessary
import flickrapi
import os
import urllib
import wget
import time
import datetime
import json

api_key="ffff563f05fae4dddb4b1cc588af7f93"
secret="a38d48a75e375712"
word="green" #<- word to search
per_page=100 # max

output="output"
size_image=3 # -1 -> max large image, 0 -> small image

num_links_to_download=1000 # number of images to download
num_pages=num_links_to_download / per_page

class FileSaver:
    def __init__(self,filename='dbpython.txt'):
        self.filename=filename

    def save(self,url):
        with open(self.filename,'a+') as f:
            f.write(url+'\n')

def getTimeStamp():
	ts = time.time()
	st = datetime.datetime.fromtimestamp(ts).strftime('%m%d_%H%M%S')
	return str(st)

saverFile = FileSaver()
flickr = flickrapi.FlickrAPI(api_key,secret,format='json')

parsed_text= urllib.quote_plus(word)

for page in range(1,num_pages):
    results = flickr.photos.search(text=parsed_text,per_page=per_page,page=page)

    destDirectory=str(output + os.sep + getTimeStamp()+"_"+parsed_text)
    if not os.path.isdir(destDirectory):
        os.makedirs(destDirectory)

    if results==None:
        print "It was not received any result"
        exit()

    parsedResults = json.loads(results)
    parsedResults = parsedResults.get("photos").get("photo")

    ids = [result.get("id") for result in parsedResults]
    if ids!=None:
        for id in ids:
            info = flickr.photos.getSizes(photo_id=id)
            if info !=None:
                try:
                    jsonValue = json.loads(info)
                    #print info
                    url = jsonValue.get("sizes").get("size")[size_image].get("source")
                    print "\nDownloading link...."+str(url)+ " in dir="+str(destDirectory)
                    wget.download(url,out=destDirectory)
                    # saverFile.save(url)
                except Exception as e:
                    print str(e)




