#!/usr/bin/python
import unittest
from google_image_feeder import KeyReader
from google_image_feeder import GoogleScrapper

class TestGoogleImageFeeder(unittest.TestCase):
    def test_file_reader(self):
        reader = KeyReader()
        self.assertEqual([True,True],reader.enables)
        self.assertTrue(len(reader.keys)==2)
    def test_google_scrapper(self):
        reader = KeyReader()
        scrapper = GoogleScrapper(10,"./test_resources",reader)
        scrapper.run("messi",1)
    def test_sizes_google_scrapper(self):
        reader = KeyReader()
        scrapper = GoogleScrapper(10,"./test_resources",reader)
        availableSizes = ["huge" "icon" "large" "medium" "xlarge" "xxlarge"]
        
        scrapper.addSize("large")
        scrapper.addSize("medium")

        scrapper.run("messi",1)



if __name__ == '__main__':
    unittest.main()
